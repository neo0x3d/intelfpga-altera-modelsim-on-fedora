# altera 16.1 - modelsim 10.4b on Fedora 23/24 (deprecated, look for up to date below)

How to install Altera ModelSim 16.0 on Fedora 23/24.<br>
There is a known problem with the current freetype packet: <https://wiki.archlinux.org/index.php/Altera_Design_Software#With_freetype2_2.5.0.1-1>

1. Install dependencies (Fedora 24)

  ```
  sudo dnf install glibc.i686 zlib.i686 bzip2-libs.i686 libXft.i686 libXext.i686 ncurses-compat-libs.i686
  ```

2. Download and install ModelSim
3. modify vsim Modify the file /home/$USER/altera/16.0/modelsim_ase/bin/vsim, row 205 from vco="linux_rh60" to vco="linux"
4. Download old freetype package, extract, copy lib

  ```
  wget ftp://rpmfind.net/linux/sourceforge/s/sl/sl7-i686-project/yum/FEDOREL7/FULLMISSING/freetype-2.4.11-9.el7.i686.rpm
  rpm2cpio freetype-2.4.11-9.el7.i686.rpm | cpio -idmv
  cp usr/lib/* /home/$USER/altera/16.0/modelsim_ase/lib/
  rm -r usr freetype-2.4.11-9.el7.i686.rpm
  ```

  _links is broken :( see new guide for Fedora 27 below_

5. Set alias for vsim

  ```
  export PATH=$PATH:/home/$USER/altera/16.0/modelsim_ase/bin
  alias vsim="LD_PRELOAD=\"/home/$USER/altera/16.0/modelsim_ase/lib/libfreetype.so.6\" vsim"
  ```

  Or use lazy.sh for automated lib installation after ModelSim has been installed.

# altera/intelFPGA 16.1 - modelsim 10.5b on Fedora 25 (probably deprecated, look for up to date below)

Walk though the steps from above (modelsim 10.4b on Fedora 23/24), after intelFPGA has been installed:

- Download and extract the same libfreetype packet as above (the version of libfreetype matters, the package for Fedora 23 or 24 can also be used on Fedora 25, it will only be used for modelsim and NOT for the rest of the system!)
- Modify the string "linux_rh60" to "linux" in the ~/intelFPGA/16.1/modelsim_ase/bin/vsim file

Add to ~/.bashrc

```
export PATH=$PATH:/home/$USER/intelFPGA/16.1/modelsim_ase/linuxaloem
alias vsim="LD_PRELOAD=\"/home/$USER/intelFPGA/16.1/modelsim_ase/lib/libfreetype.so.6\" vsim"
```

# intelFPGA_pro 17.1 - modelsim 10.5c on Fedora 27 XFCE

1. Install dependencies as shown before (see guide for modelsim 10.4b)
2. Download and install modelsim (does not need root permission)
3. The line which needs to be modified in the "vsim" file is now in row 210 instead of 205. (see guide for modelsim 10.4b)

1. Download freetype package to intelFPGA dir, unpack it
```
cd /home/$USER/intelFPGA/17.1
wget https://rpmfind.net/linux/centos/7/os/x86_64/Packages/freetype-2.4.11-15.el7.i686.rpm
rpm2cpio freetype-2.4.11-15.el7.i686.rpm | cpio -idmv
```

1. in ~/.bashrc set alias vor vsim, start program from terminal. Add this text to your ~/.bashrc
```
export PATH=$PATH:/home/$USER/intelFPGA_pro/17.1/modelsim_ase/bin
alias vsim="LD_PRELOAD=\"/home/$USER/intelFPGA_pro/17.1/usr/lib/libfreetype.so.6\" vsim"
```

# intelFPGA_pro 18.1 - modelsim 10.6d on Fedora 29 GNOME

1. Install dependencies as shown before (see guide for modelsim 10.4b)
2. Download and install the two parts from http://fpgasoftware.intel.com/ (ModelSimProSetup-18.1.0.222-linux.run and modelsim-part2-18.1.0.222-linux.qdz,
download both, install via executing the first one)
3. In modelsim_ase/bin/vsim, change vco="linux_rh60" to vco="linux"

No other steps are required, start Modelsim executable via terminal "/home/$USER/intelFPGA_pro/18.1/modelsim_ase/bin/vsim", or create a Desktop entry:

cat <<EOT >> /home/$USER/.local/share/applications/Modelsim10.6d.desktop
#!/usr/bin/env xdg-open
[Desktop Entry]
Type=Application
Icon=
Name=ModelSim 10.6d
Comment=intelFPGA Pro 18.1 - ModelSim 10.6d
Exec=/home/$USER/intelFPGA_pro/18.1/modelsim_ase/bin/vsim
Categories=Development;
EOT

Don't forget to make .desktop entry executable
	chmod +x /home/$USER/.local/share/applications/Modelsim10.6d.desktop

Edit: still shows inconsistent behavor. Application can be started via vsim command but desktiop shortcut does not work consistently
